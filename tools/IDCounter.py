class IDCounter:

  def __init__(self):
    self.id = 0

  def getNextID(self):
    self.id += 1
    return(self.id)